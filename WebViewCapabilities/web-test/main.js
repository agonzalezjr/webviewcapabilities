function myAlert(msg) {
  alert(msg);
}

function myLog(msg) {
  console.log(msg);
}

function sendDataToNative() {
  var value = document.getElementById("dataInput").value;
  if (
    window.webkit &&
    window.webkit.messageHandlers &&
    window.webkit.messageHandlers.bridge
  ) {
    window.webkit.messageHandlers.bridge.postMessage(value);
  } else {
    myAlert("No bridge available");
  }
}

function syncThree() {
  console.log("Asking for syncThree ...");
  console.log("Returning syncThree.");
  return 3;
}

function asyncSix() {
  console.log("Asking for asyncSix ...");
  return new Promise(function(resolve) {
    setTimeout(function() {
      console.log("Returning asyncSix.");
      resolve(6);
    }, 3000);
  });
}

async function awaitAsyncSix() {
  let asyncValue = await awaitAsyncSix();
  return asyncValue;
}

async function populateSyncAsyncData() {
  let syncValue = syncThree();
  document.getElementById("sync").innerText = "sync: " + syncValue;
  console.log(">>> calling await/async")
  let asyncValue = await asyncSix();
  document.getElementById("async").innerText = "async: " + asyncValue;
  console.log(">>> calling over the bridge")
  window.webkit.messageHandlers.valuesReady.postMessage({ values: [syncValue, asyncValue]})
  console.log(">>> bridge call done")
}
