//
//  ViewController.swift
//  WebViewCapabilities
//
//  Created by Andres Gonzalez Jr on 10/8/19.
//  Copyright © 2019 mirata. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var sendToWebTextField: UITextField!
    @IBOutlet weak var gotFromWebTextField: UITextView!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var dataSumButton: UIButton!
    
    var values: [Int] = []
    
    private var webView: WKWebView?
    
    @IBAction func sendToWeb(_ sender: Any) {
        let jsCode = """
        document.getElementById("fromNative").innerText = "\(sendToWebTextField.text!)"
        """
        runJSCode(jsCode)
    }
    
    // Runs JS code async, just like "evaluateJavaScript" does
    func runJSCode(_ jsCode: String) {
        print("➡️ webView evaluateJavaScript: \(jsCode)")
        webView!.evaluateJavaScript(jsCode) { (result, error) in
            print("➡️ webView asyncEvaluation complete")
            if let myError = error {
                print("  🛑 error: \(myError.localizedDescription)")
            } else {
                print("  ✅ result: \(result ?? "-- no result --")")
            }
        }
        print("➡️ runJSCode() complete")
    }
    
    // Runs JS code sync
    func runJSCodeSync(_ jsCode: String) throws -> Any? {
        var myResult: Any? = nil
        var myError: Error? = nil
        
        let group = DispatchGroup()
        group.enter()
        
        print("➡️ webView evaluateJavaScript: \(jsCode)")
        
        self.webView!.evaluateJavaScript(jsCode) { (result, error) in
            print("➡️ webView asyncEvaluation complete")
            myResult = result
            myError = error
            group.leave()
        }
        
        let waitResult = group.wait(timeout: .now() + 15 /* seconds */)
        
        if (waitResult == .timedOut) {
            print("➡️ runJSCodeSync() timed out")
            return nil
        } else {
            print("➡️ runJSCodeSync() complete")
            if let error = myError {
                print("  🛑 error: \(error.localizedDescription)")
            } else {
                print("  ✅ result: \(myResult ?? "-- no result --")")
            }
            
            if let theError = myError {
                throw theError
            }
            return myResult
        }
    }
    
    let syncJSCall = """
        syncThree()
    """
    
    @IBAction func runSyncJSCode(_ sender: Any) {
        runJSCode(syncJSCall)
    }
    
    @IBAction func runSyncJSCodeSync(_ sender: Any) {
        do {
            let result = try runJSCodeSync(syncJSCall)
            print("🔁 sync eval done - resut: \(String(describing: result))")
        } catch let error {
            print("🔁 sync eval done - error: \(error.localizedDescription)")
        }
    }

    let asyncJSCall = """
        awaitAsyncSix()
    """

    @IBAction func runAsyncJSCode(_ sender: Any) {
        runJSCode(asyncJSCall)
    }
        
    @IBAction func runAsyncJSCodeSync(_ sender: Any) {
//        do {
//            let result = try runJSCodeSync(asyncJSCall)
//        } catch error {
//
//        }
    }
    
    @IBAction func getDataSum(_ sender: Any) {
        // Need to call to get sync-3 and async-6 from JS and
        // return their sum ...
        let sum = values.reduce(0, +)
        sumLabel.text = "\(sum)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NOTE: WKUserScript can be use to run code whenever the page is finished loading
        // We are hijacking console.log() here so we can see all those logs in Xcode
        // We can probably modify the DOM with this as well ;)
        let initJSCode = """
            /* document.body.style.background = "#FFB6C1"; */

            var console = {
                log: function(msg) {
                    window.webkit.messageHandlers.logging.postMessage(msg)
                }
            };

            console.log("initJSCode is done")
        """
        
        let userScript = WKUserScript(source: initJSCode,
                                      injectionTime: .atDocumentEnd,
                                      forMainFrameOnly: true)
        
        let userContentController = WKUserContentController()
        
        userContentController.add(self, name: "bridge")
        userContentController.add(self, name: "logging")
        userContentController.add(self, name: "valuesReady")
        userContentController.add(self, name: "formsBridge")
        userContentController.addUserScript(userScript)
        
        let webViewConfiguration = WKWebViewConfiguration()
        webViewConfiguration.userContentController = userContentController
        
        webView = WKWebView(frame: webViewContainer.bounds, configuration: webViewConfiguration)
        webView?.layer.borderWidth = 3
        webView?.layer.borderColor = UIColor.red.cgColor
        webView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        webViewContainer.addSubview(webView!)
        
        webView?.uiDelegate = self
        webView?.navigationDelegate = self
        
        // Loading from the web
        /*
         let myURL = URL(string:"https://www.apple.com")
         let myRequest = URLRequest(url: myURL!)
         webView.load(myRequest)
         */
        
        // Loading from a local file
//        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "web-test")!
//        webView?.loadFileURL(url, allowingReadAccessTo: url)
        
        // Loading from a local file (the renderer: works!)
         let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "web-renderer")!
         webView?.loadFileURL(url, allowingReadAccessTo: url)
    }
    
    // MARK: WKUIDelegate
    
    func webViewDidClose(_ webView: WKWebView) {
        print("➡️ webView didClose")
    }
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        print("➡️ webView runJSAlert - message: '\(message)'")
        completionHandler()
    }
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // This will run AFTER the WKUserScript is done running
        print("➡️ webView didFinish navigation - url: \(webView.url!)")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("➡️ webView didFail navigation - error: \(error.localizedDescription)")
    }
    
    // MARK: WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // print("➡️ didReceive from webView - message: '\(message.body)'")
        if (message.name == "bridge") {
            gotFromWebTextField.text = String(describing: message.body)
        }
        if (message.name == "logging") {
            print("✏️ console.log - \(message.body)")
        }
        if (message.name == "valuesReady") {
            print("↔️ Values are ready ... getting them ...")
            self.webView?.evaluateJavaScript("getValues()") { (result, error) in
                self.values = (message.body as! [String: [Int]])["values"]!
                self.dataSumButton.isEnabled = true
            }
        }
        if (message.name == "formsBridge") {
            let body: [String: Any] = message.body as! [String: Any]
            print("↔️ message.type = \(body["type"] as! String)")
            let dict: JSONDictionary = body["data"] as! JSONDictionary
            gotFromWebTextField.text = asString(jsonDictionary: dict)
        }
    }
    
    typealias JSONDictionary = [String : Any]

    func asString(jsonDictionary: JSONDictionary) -> String {
      do {
        let data = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        return String(data: data, encoding: String.Encoding.utf8) ?? ""
      } catch {
        return ""
      }
    }

}

